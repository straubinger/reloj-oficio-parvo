import { persisted } from 'svelte-local-storage-store'

class Valued {
    stringify(val: any) {
        return val
    }

    parse(val: any) {
        return val
    }
}

const keyLang = "lang"
const keyTimezone = "timezone"

const defaultLang = "es"
const defaultTimezone = "America/Costa_Rica"

export const lang = persisted(keyLang, defaultLang, { serializer: new Valued() })
export const timezone = persisted(keyTimezone, defaultTimezone, { serializer: new Valued() })
