//import { dawn, dusk, night } from "suncalc";
import type { Datetime } from "$lib/datetime/time";
import type { Dayjs } from "dayjs";
import type dayjs from "dayjs";

enum SunAt {
    Ignore = -1,
    Dusk = 0,
    Dawn,
    Night,
}

export enum Hour {
    Unknown = 0,
    Maitins,
    Laudes,
    Prime,
    Terce,
    Sext,
    None,
    Vesper,
    Compline,
}

export enum Office {
    First = 1,
    Second,
    Third
}

const hours = {
    [Hour.Maitins]: [SunAt.Ignore, 0, 2],
    [Hour.Laudes]: [SunAt.Dawn, 0, 5],
    [Hour.Prime]: [SunAt.Dawn, 1.5, 6],
    [Hour.Terce]: [SunAt.Ignore, 0, 9],
    [Hour.Sext]: [SunAt.Ignore, 0, 12],
    [Hour.None]: [SunAt.Ignore, 0, 15],
    [Hour.Vesper]: [SunAt.Dusk, 0, 18],
    [Hour.Compline]: [SunAt.Night, 0, 19],
};

const listOfHours = [
    Hour.Maitins,
    Hour.Laudes,
    Hour.Prime,
    Hour.Terce,
    Hour.Sext,
    Hour.None,
    Hour.Vesper,
    Hour.Compline,
];

// get1stSundayOfAdvent obtiene la fecha del primer domingo de Adviento
export const get1stSundayOfAdvent = (
    year: number,
    datetime: Datetime
): dayjs.Dayjs => {
    let sunday = datetime.mkDate(year, 12, 24, 0, 0, 0, true).subtract({ day: 21 })

    while (sunday.day() !== 0) {
        sunday = sunday.subtract({ day: 1 })
    }

    return sunday
};

const makeDateHour = (
    now: dayjs.Dayjs,
    hour: number,
    datetime: Datetime
): Dayjs => {
    return datetime.mkDate(now.year(), now.month() + 1, now.date(), hour, 0, 0, true)
};

const getFixHours = (now: dayjs.Dayjs, timezone: Datetime): Record<Hour, dayjs.Dayjs> => {
    return {
        [Hour.Maitins]: makeDateHour(now, hours[Hour.Maitins][2], timezone),
        [Hour.Laudes]: makeDateHour(now, hours[Hour.Laudes][2], timezone),
        [Hour.Prime]: makeDateHour(now, hours[Hour.Prime][2], timezone),
        [Hour.Terce]: makeDateHour(now, hours[Hour.Terce][2], timezone),
        [Hour.Sext]: makeDateHour(now, hours[Hour.Sext][2], timezone),
        [Hour.None]: makeDateHour(now, hours[Hour.None][2], timezone),
        [Hour.Vesper]: makeDateHour(now, hours[Hour.Vesper][2], timezone),
        [Hour.Compline]: makeDateHour(now, hours[Hour.Compline][2], timezone),
        [Hour.Unknown]: makeDateHour(now, 23, timezone),
    };
};

const getStopHour = (current: Hour): Hour => {
    switch (current) {
        case Hour.Maitins:
            return Hour.Laudes;
        case Hour.Laudes:
            return Hour.Prime;
        case Hour.Prime:
            return Hour.Terce;
        case Hour.Terce:
            return Hour.Sext;
        case Hour.Sext:
            return Hour.None;
        case Hour.None:
            return Hour.Vesper;
        case Hour.Vesper:
            return Hour.Compline;
        case Hour.Compline:
            return Hour.Unknown;
    }
    return Hour.Unknown;
};

// getTime compara la hora actual del día con las horas de inicio del amanecer,
// atardecer, y la noche para saber en qué hora canonica estamos
export const getTime = (now: Date, datetime: Datetime): Hour => {
    const fixHours = getFixHours(datetime.mkDateFromDate(now), datetime);
    const currentTime = datetime.mkDateFromDate(now)

    for (const hour of listOfHours) {
        const stopHour = getStopHour(hour);

        const start = fixHours[hour];
        const end = fixHours[stopHour];
        if (
            currentTime.isBetween(start, end, "m", "[)")
        ) {
            return hour;
        }
    }

    return Hour.Unknown;
};

export const getOffice = (now: Date, datetime: Datetime): Office => {
    const today = datetime.mkDateFromDate(now)
    const currentYear = today.year()
    const sabado = get1stSundayOfAdvent(currentYear, datetime).subtract({ day: 1 })

    // desde el 03/02 hasta el sabado antes del primer domingo de adviento antes de visperas
    const tiempo1 = datetime.inBetween(
        today,
        datetime.mkDateFromString("03/02", "DD/MM"), Hour.Maitins,
        sabado, Hour.Vesper
    )

    // desde las visperas del sabado antes del primer domingo de adviento hasta antes de las visperas del 24 de dic.
    const tiempo2 = datetime.inBetween(today, sabado, Hour.Vesper, datetime.mkDateFromString("24/12", "DD/MM"), Hour.Vesper) ||
        today.format("DD/MM") === "25/03"

    // desde las visperas del 24 de dic. hasta el 02/02 del año siguiente
    const tiempo3 = (datetime.inBetween(
        today, datetime.mkDateFromString("24/12", "DD/MM"), Hour.Vesper,
        datetime.mkDateFromString(`03/02/${currentYear + 1}`, "DD/MM/YYYY"), Hour.Maitins) ||
        today.isBefore(datetime.mkDateFromString("03/02", "DD/MM"), "day"))

    if (tiempo2) {
        return Office.Second
    }

    if (tiempo1) {
        return Office.First
    }

    if (tiempo3) {
        return Office.Third
    }
}
