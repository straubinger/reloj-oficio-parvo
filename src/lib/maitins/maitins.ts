import { Office } from "$lib/hora/hora"

const sundayPsalms = [
    8, 18, 23
]
const tuesdayPsalms = [
    44, 45, 86
]
const wednesdayPsalms = [
    59, 96, 97
]

const lessons = [
    "Eclo 24, 11-13",
    "Eclo 24, 15-16",
    "Eclo 24, 17-20"
]
const lessons2ndOffice = [
    "Lucas 1, 26-28",
    "Lucas 1, 29-33",
    "Lucas 1, 34-38"
]

export const getPsalmsLessons = (day: number, office: Office): { psalms: number[], lessons: string[] } => {
    let psalms = sundayPsalms
    if (day === 2 || day === 5) {
        psalms = tuesdayPsalms
    } else if (day === 3 || day === 6) {
        psalms = wednesdayPsalms
    }
    const currentLessons = office === Office.Second ? lessons2ndOffice : lessons

    return { psalms, lessons: currentLessons }
}
