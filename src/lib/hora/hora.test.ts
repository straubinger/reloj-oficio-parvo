import { describe, expect, it } from "vitest";
import { get1stSundayOfAdvent, getOffice, getTime, Hour, Office } from "./hora";
import { Datetime } from "$lib/datetime/time";

const dtcr = new Datetime('America/Costa_Rica')

const testExact = [
    {
        name: "Maitines a las 2 a.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 2, 0, 0, true) },
        expected: Hour.Maitins,
    },
    {
        name: "Laudes a las 5 a.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 5, 0, 0, true) },
        expected: Hour.Laudes,
    },
    {
        name: "Prime a las 6 a.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 6, 0, 0, true) },
        expected: Hour.Prime,
    },
    {
        name: "Terce a las 9 a.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 9, 0, 0, true) },
        expected: Hour.Terce,
    },
    {
        name: "Sext a las 12 m.d.",
        input: { date: dtcr.mkDate(2023, 5, 31, 12, 0, 0, true) },
        expected: Hour.Sext,
    },
    {
        name: "None a las 3 p.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 15, 0, 0, true) },
        expected: Hour.None,
    },
    {
        name: "Vesper a las 6 p.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 18, 0, 0, true) },
        expected: Hour.Vesper,
    },
    {
        name: "Compline a las 7 p.m.",
        input: { date: dtcr.mkDate(2023, 5, 31, 19, 0, 0, true) },
        expected: Hour.Compline,
    },
];

describe("getTime retorna la hora correcta con el tiempo exacto del día", () => {
    for (const cs of testExact) {
        it(`${cs.name} con fecha: ${dtcr.format(cs.input.date)}`, () => {
            expect(getTime(cs.input.date.toDate(), dtcr)).toBe(cs.expected);
        });
    }
});

describe("geTime retorna la hora correcta con el tiempo del día un poco pasado", () => {
    for (const cs of testExact) {
        const time = cs.input.date.add({ minute: 25 })
        it(`${cs.name} con fecha: ${dtcr.format(time)}`, () => {
            expect(getTime(time.toDate(), dtcr)).toBe(cs.expected);
        });
    }
});

const testAdventDates = [
    {
        name: 2023,
        expect: dtcr.mkDate(2023, 12, 3, 0, 0, 0, true),
    },
    {
        name: 2013,
        expect: dtcr.mkDate(2013, 12, 1, 0, 0, 0, true),
    },
    {
        name: 2012,
        expect: dtcr.mkDate(2012, 12, 2, 0, 0, 0, true),
    },
    {
        name: 2011,
        expect: dtcr.mkDate(2011, 11, 27, 0, 0, 0, true),
    },
];

describe("get1stSundayOfAdvent calcula la fecha del primer domingo de Adviento", () => {
    for (const cs of testAdventDates) {
        it(`Primer domingo de Adviento del año ${cs.name}`, () => {
            expect(dtcr.format(get1stSundayOfAdvent(cs.name, dtcr))).toBe(dtcr.format(cs.expect));
        });
    }
});

const testOfficeDates = [
    {
        name: "Hoy esta en el tiempo 1 (2 días despues del 3 de feb)",
        input: { date: dtcr.mkDate(2023, 2, 6, 14, 25, 0, true) },
        expect: Office.First
    },
    {
        name: "Hoy esta en el tiempo 1 (al inicio)",
        input: { date: dtcr.mkDate(2023, 2, 3, 2, 0, 0, true) },
        expect: Office.First
    },
    {
        name: "Hoy es la Anunciacion",
        input: { date: dtcr.mkDate(2023, 3, 25, 14, 25, 0, true) },
        expect: Office.Second
    },
    {
        name: "Hoy esta en el tiempo 2",
        input: { date: dtcr.mkDate(2023, 12, 18, 14, 25, 0, true) },
        expect: Office.Second
    },
    {
        name: "Hoy esta en el tiempo 3 (estamos en enero)",
        input: { date: dtcr.mkDate(2023, 1, 18, 14, 25, 0, true) },
        expect: Office.Third
    },
    {
        name: "Hoy esta en el tiempo 3 (es Navidad)",
        input: { date: dtcr.mkDate(2023, 12, 25, 14, 25, 0, true) },
        expect: Office.Third
    }
]

describe("getOffice indica que oficio debe ser rezado", () => {
    for (const cs of testOfficeDates) {
        it(cs.name, () => {
            expect(getOffice(cs.input.date.toDate(), dtcr)).toBe(cs.expect)
        })
    }
})
