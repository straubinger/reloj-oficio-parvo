import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import objectSupport from "dayjs/plugin/objectSupport";
import customParseFormat from 'dayjs/plugin/customParseFormat'
import localizedFormat from "dayjs/plugin/localizedFormat";
import 'dayjs/locale/en'
import 'dayjs/locale/es'
import 'dayjs/locale/pt'

import { getTime, type Hour } from "$lib/hora/hora";

dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(isBetween);
dayjs.extend(localizedFormat);
dayjs.extend(objectSupport);
dayjs.extend(customParseFormat);

export class Datetime {
    private tz: string;
    private lang: string;

    constructor(timezone: string | null, locale: string | null) {
        this.tz = timezone ? timezone : "America/Costa_Rica";
        this.lang = locale ? locale : "es";
    }

    public mkDate(
        year: number,
        month: number,
        day: number,
        hour?: number,
        minute?: number,
        second?: number,
        keep?: boolean,
    ): dayjs.Dayjs {
        const time = dayjs({ year, month: month - 1, day, hour, minute, second }).tz(
            this.tz,
            keep
        ).locale(this.lang);

        return time
    }

    public mkDateFromDate(date: Date, keep?: boolean): dayjs.Dayjs {
        return dayjs(date).tz(this.tz, keep).locale(this.lang)
    }

    public mkDateFromString(date: string, format: string): dayjs.Dayjs {
        return dayjs(date, format).locale(this.lang)
    }

    public format(time: dayjs.Dayjs): string {
        return time.format("llll")
    }

    public inBetween(date: dayjs.Dayjs, start: dayjs.Dayjs, startHour: Hour, end: dayjs.Dayjs, endHour: Hour): boolean {
        const atStart = start.format('DD/MM/YYYY') === date.format('DD/MM/YYYY') && getTime(date.toDate(), this) >= startHour
        const atEnd = end.format('DD/MM/YYYY') === date.format('DD/MM/YYYY') && getTime(date.toDate(), this) < endHour

        return (atStart || date.isAfter(start)) && (atEnd || date.isBefore(end))
    }
}
